#!/usr/bin/env bash
# set -x
# set -v

export ACCOUNT_NAME="beesbank"
export REPO_NAME="bank-services-service"
export JIRA_PROJECT_KEY="SVC"
export CHANGELOG_FILE="SVC_CHANGELOG"

if [ "$#" = "0" ]; then
    echo "NO Jira issue provided (first argument of the script)"
    export JIRA_ISSUE=""
    export JIRA_ISSUE_PREFIX=""
else
    export JIRA_ISSUE="$1"
    export JIRA_ISSUE_PREFIX="[$1]"
    echo "Use jira issue $JIRA_ISSUE"
fi

git pull

touch .fail-in-release
git add .fail-in-release

echo "$(date) $JIRA_ISSUE_PREFIX Break release" >> $CHANGELOG_FILE
git add $CHANGELOG_FILE

git commit -m "$JIRA_ISSUE_PREFIX Break Release" .fail-in-release $CHANGELOG_FILE

git push

echo "Release pipeline broken"