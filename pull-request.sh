#!/usr/bin/env bash
# set -x

export ACCOUNT_NAME="beesbank"
export REPO_NAME="bank-services-service"
export JIRA_PROJECT_KEY="SVC"
export CHANGELOG_FILE="SVC_CHANGELOG"

# https://developer.atlassian.com/jiradev/jira-apis/jira-rest-apis/jira-rest-api-tutorials/jira-rest-api-example-create-issue

export FEATURE_DESC="Feature $(date)"

export JIRA_ISSUE_AS_JSON=$(curl --silent --netrc -H "Content-Type: application/json" -X POST \
   --data "{\"fields\":{\"project\":{\"key\":\"$JIRA_PROJECT_KEY\"},\"summary\":\"$FEATURE_DESC\",\"description\":\"We are working on $FEATURE_DESC\",\"issuetype\":{\"name\":\"New Feature\"}}}" \
   https://jira.beescloud.com/rest/api/2/issue/)


export JIRA_ISSUE=$(echo $JIRA_ISSUE_AS_JSON | jq -r .key)

echo "Issue $JIRA_ISSUE created"

# TODO update issue, set status to "in progress"
# see https://docs.atlassian.com/jira/REST/server/?_ga=2.148590398.82631418.1503650538-983538049.1489593855#api/2/issue-doTransition


export COMMIT_MESSAGE="$JIRA_ISSUE change"

git pull

git checkout -b "$JIRA_ISSUE"

echo "Git branch $JIRA_ISSUE created"


echo "$(date) [$JIRA_ISSUE] $COMMIT_MESSAGE" >> $CHANGELOG_FILE

git add $CHANGELOG_FILE
git commit -m "[$JIRA_ISSUE] $COMMIT_MESSAGE"

git push --set-upstream origin "$JIRA_ISSUE"

git checkout master


# https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/pullrequests
# /2.0/repositories/beesbank/$REPO_NAME/pullrequests

# INFO: this curl call requires authentication through ~/.netrc
# SAMPLE OF .netrc
# cat ~/.netrc
# machine api.bitbucket.org login <<bitbucket username such as 'cleclerc'>> password << password >>

export BITBUCKET_PR=$(curl --silent --netrc -H "Content-Type: application/json" -X POST \
   -d "{\"title\":\"[$JIRA_ISSUE] $COMMIT_MESSAGE\",\"source\":{\"branch\":{\"name\":\"$JIRA_ISSUE\"}},\"destination\":{\"branch\":{\"name\":\"master\"}}}" \
   https://api.bitbucket.org/2.0/repositories/$ACCOUNT_NAME/$REPO_NAME/pullrequests/)

# sample json: https://gist.github.com/cyrille-leclerc/f35774a0c63e3816f7ccc99f1181d74c#file-bitbucket-pull-request-creation-response-json

export BITBUCKET_PR_ID=$(echo $BITBUCKET_PR | jq -r .id)
export BITBUCKET_PR_MERGE_URL=$(echo $BITBUCKET_PR | jq -r .links.merge.href)
export BITBUCKET_PR_URL=$(echo $BITBUCKET_PR | jq -r .links.html.href)

echo "Created PR $BITBUCKET_PR_ID"
echo "$BITBUCKET_PR_URL"

read -p "Press enter to merge PR $BITBUCKET_PR_ID used for $JIRA_ISSUE ? " -n 1 -r

export BITBUCKET_PR_MERGE_RESPONSE=$(curl --silent --netrc -H "Content-Type: application/json" -X POST \
   -d "{\"close_source_branch\": true}" \
   $BITBUCKET_PR_MERGE_URL)

# json response: https://gist.github.com/cyrille-leclerc/f35774a0c63e3816f7ccc99f1181d74c#file-bitbucket-pull-request-merge-response-json

git pull

echo "## SUCCESS #"
echo ""
echo "PR $BITBUCKET_PR_ID for $JIRA_ISSUE merged"
echo ""
echo "$BITBUCKET_PR_URL"
echo ""
