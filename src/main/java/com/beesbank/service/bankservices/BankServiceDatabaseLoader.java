package com.beesbank.service.bankservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BankServiceDatabaseLoader implements CommandLineRunner {

    private final BankServiceRepository repository;

    @Autowired
    public BankServiceDatabaseLoader(BankServiceRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... strings) throws Exception {
        this.repository.save(new BankService("Cash withdrawal", "Withdraw cash"));
        this.repository.save(new BankService("Money Transfer", "Transfer money"));
    }
}