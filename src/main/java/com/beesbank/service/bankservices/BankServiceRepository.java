package com.beesbank.service.bankservices;

import org.springframework.data.repository.CrudRepository;

public interface BankServiceRepository extends CrudRepository<BankService, Long> {
}
