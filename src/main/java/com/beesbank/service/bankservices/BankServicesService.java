package com.beesbank.service.bankservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankServicesService {

    public static void main(String[] args) {
        SpringApplication.run(BankServicesService.class, args);
    }
}
